import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, StatusBar } from 'react-native';
import { Container, Tab, Tabs } from 'native-base';
import io from 'socket.io-client'


const socket = io('http://192.168.1.38:3000');

export default class App extends Component {
  
  componentDidMount() {
    // const script = document.createElement("script");
    // script.src = '/node_modules/socket.io-client/dist/socket.io.js';
    // script.async = true;
    // document.body.appendChild(script);

    socket.on('connect', () => {
      alert(socket.connected); // true
      alert(socket.id);
    });
  }


  render() {
    return (

      <SafeAreaView style={styles.safeArea}>
        <StatusBar barStyle='light-content' />
        <Container>
          <Tabs tabBarPosition="bottom">
            <Tab heading="Tab1">

            </Tab>
            <Tab heading="Tab2">

            </Tab>
            <Tab heading="Tab3">

            </Tab>
          </Tabs>
        </Container>
      </SafeAreaView>
    );
  }

}

const styles = StyleSheet.create({
  safeArea: {
    flex: 5,
    backgroundColor: '#2b2b2b',
    color: 'white',
  }
})


